# WBLNA Design Docs

## Input Matching Network

The current design uses a quasi-suspended-substrate design for exceptionally low loss.
Current research transforms this into a double-quasi-suspended-substrate to reduce loss even further.

This uses parallel traces, stitched with vias to reduce E-field in the dielectric and increases the total copper surface area.

Here is a drawing of the HFSS model (with an H-Plane symmetry condition)
![](DQSS.png)

The critical dimensions are:
Trough dimensions - 6x6mm
Cover height - 10mm

And then the loss and impedance of different widths is seen here:

![](DQSS_Params.png)

The data associated with this is included in this repo.

To compute a width from an impedance, you can use the curve-fit expression

Z(W) = 24.4719 * exp(-0.0159*W) - 0.6172