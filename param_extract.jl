using CSV, LsqFit, Plots

data = CSV.File("WBLNA_Tline_Data.csv")

ads_impedances = [89, 70, 70, 70, 70, 70, 70, 70, 70, 95, 121, 126, 133, 130, 70, 70, 70, 114, 169, 196, 199, 200, 200, 200, 200, 200, 200, 200, 200, 200]

m(t, p) = p[1] * exp.(p[2] * t) .+ p[3]
p0 = [0.0, 0.0, 0.0]
fit = curve_fit(m, data["z0"], data["width"], p0)

model(x) = m(x, [24.4719, -0.0159, -0.6172])

plot(data["z0"], data["width"])
plot!(data["z0"], model.(data["z0"]))

model.(ads_impedances)